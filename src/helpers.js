import MarkdownIt from "markdown-it";
import hljs from "highlight.js";

// Initialize MarkdownIt once and export for reuse
export const md = new MarkdownIt({
  html: true,
  linkify: true,
  typographer: true,
  highlight: function (code) {
    const result = hljs.highlightAuto(code);
    return `<pre><code class="hljs">${result.value}</code></pre>`;
  },
});

export function renderMarkdown(markdownText) {
  if (!md) {
    console.error("Markdown parser is not initialized.");
    return "";
  }

  return md.render(markdownText);
}

export function outputResult(resultArray) {
  let resultMsg = resultArray.join("");
  return md ? renderMarkdown(resultMsg) : resultMsg;
}
