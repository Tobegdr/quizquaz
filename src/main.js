import { createApp } from "vue";
import store from "./store";
import axios from "axios"; // Import your axios instance
import App from "./App.vue";
import "bootstrap/dist/js/bootstrap.min.js";
import "./assets/styles/app.scss";

// Font Awesome setup
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons'; // Import all solid icons
import { far } from '@fortawesome/free-regular-svg-icons'; // Import all regular icons
import { fab } from '@fortawesome/free-brands-svg-icons'; // Import all brand icons
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

// Add all icons to the library
library.add(fas, far, fab);

const app = createApp(App);

app.use(store);

// provide axios globally
app.config.globalProperties.$axios = axios;

// Register FontAwesome globally
app.component('font-awesome-icon', FontAwesomeIcon);

app.mount("#app");
