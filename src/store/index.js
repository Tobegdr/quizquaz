import { createStore } from "vuex";
import data from "../data.json";

const store = createStore({
  state() {
    return {
      allQuizes: data,
      quizes: [],
      step: 0,
      finished: false,
    };
  },
  mutations: {
    SET_QUIZES(state, quizes) {
      state.quizes = quizes;
    },
    INCREMENT_STEP(state) {
      if (state.step < state.quizes.length - 1) {
        state.step += 1;
      } else {
        state.finished = true;
      }
    },
    DECREMENT_STEP(state) {
      if (state.step > 0) {
        state.step -= 1;
      }
    },
    RESET_QUIZ(state) {
      state.quizes = [];
      state.step = 0;
      state.finished = false;
      state.allQuizes.forEach((quiz) => {
        quiz.quizes.forEach((q) => {
          q.correct = undefined;
          q.optionIndex = undefined;
        });
      });
    },
    CHECK_ANSWER(state, { option, index }) {
      const currentQuiz = state.quizes[state.step];
      if (option === currentQuiz.answer) {
        currentQuiz.correct = true;
      } else {
        currentQuiz.correct = false;
      }
      currentQuiz.optionIndex = index;
    },
    ADD_QUIZ(state, quizData) {
      state.allQuizes.push(quizData);
    },
    STOP_QUIZ(state) {
      state.quizes = [];
    },
  },
  actions: {
    setQuizes({ commit }, quizes) {
      commit("SET_QUIZES", quizes);
    },
    incrementStep({ commit }) {
      commit("INCREMENT_STEP");
    },
    decrementStep({ commit }) {
      commit("DECREMENT_STEP");
    },
    resetQuiz({ commit }) {
      commit("RESET_QUIZ");
    },
    checkAnswer({ commit }, payload) {
      commit("CHECK_ANSWER", payload);
    },
    addQuiz({ commit }, quizData) {
      commit("ADD_QUIZ", quizData);
    },
    stopQuiz({ commit }) {
      commit("STOP_QUIZ");
    },
  },
  getters: {
    quizes: (state) => state.quizes,
    step: (state) => state.step,
    finished: (state) => state.finished,
    allQuizes: (state) => state.allQuizes,
    hidePrevious: (state) => state.step === 0,
    hideNext: (state) => state.quizes && state.step === state.quizes.length - 1,
  },
});

export default store;
